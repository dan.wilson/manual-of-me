# Manual of me

## What is this?
With the nature of remote work it's hard to get a grasp of who people are and how they like to work. See this as documentation for working with me. I hope this gives you a better understanding of who I am and how we can best work together. See [manualof.me](https://www.manualof.me) for more details. 


## The basic you should know about me
* I value creating value, not pushing tickets through a board
* If I’m asking questions, it’s for clarification purposes and extra context
* I’m an expert, as are you, and expect to be treated like one
    * Read first point again
* My nearest and dearest (mother and girlfriend) told me that I sound very grumpy on the phone. If I come across blunt then I’m apologise. At the end of the day, we share the same goal; **creating valuable software for our users.** 
    * This is my main intention
* I use emacs


## The best way to give me feedback
* Is in two stages: 
    * First stage would be async. It take a me a while to digest information and I do this best in solitary. Doesn’t need to be any specific length, we will discuss to whatever length you like there after.
    * Second stage would be coming together for a synchronous chat about it in a bit more detail.
* I don’t want this to sound like I’ll be going away to prepare my defence. It really is that I want to fully digest the information first. I will struggle to take it on board from a synchronous conversation. 


## My best working patterns look like
* Pomodoro life 🍅 I love this method of working, it allows me to focus fully on one thing at time and give my brain the freedom of a little break in between.
* I like to do one thing, at time keeping my focus narrow and sharp
* You won’t catch me out of hours and I shouldn’t you either


## Communications channels
* Async and open communication above all else
    * Mention me on Jira, assign me a merge request and blast me with comments on a code review
* Teams secondary, happy to ad-hoc video call as well
* What’s an email?
* [An article](https://ben.balter.com/2014/11/06/rules-of-communicating-at-github/) of rules that I try to adhear to


## Beyond work I’m passion about
* I play only one game; [StarCraft II - Wikipedia](https://en.wikipedia.org/wiki/StarCraft_II)
    * I’m very excited for [Stormgate - Wikipedia](https://en.wikipedia.org/wiki/Stormgate)
* I’m a regular at the climbing gym
* I love going hiking and being outdoors
* I converted a [Renault Kangoo - Wikipedia](https://en.wikipedia.org/wiki/Renault_Kangoo) into a camper van and travelled round Europe in it for 3 weeks
* I dream of getting a bigger van soon
